#!/bin/bash
# Installer script for gnu taler libs needed for mdb-taler project on Raspbian

# Check if installpath is valid
if [ ! "$1" ]
	then
		echo "Installing in $HOME/gnuTalerLibs"; 
		INSTALLPATH="$HOME/gnuTalerLibs"
elif [ ! -d "$1" ]
	then
		echo "Error: $1 is not a directory"; 
		exit 1;
else
	echo "Installing in /$1";
	INSTALLPATH=$1;
	INSTALLPATH+="/libs";
	echo $INSTALLPATH;
fi

mkdir $INSTALLPATH
cd $INSTALLPATH

sudo sed -i 's/# de_CH.UTF-8 UTF-8/de_CH.UTF-8 UTF-8/g' /etc/locale.gen
sudo locale-gen

sudo apt-get install \
  autoconf \
  automake \
  autopoint \
  libtool \
  libltdl-dev \
  libunistring-dev \
  libcurl4-gnutls-dev \
  libgcrypt20-dev \
  libjansson-dev \
  libpq-dev \
  postgresql-9.4 \
  texinfo 
	
# Install taler dependecies
# gnurl:
	sudo apt install postgresql-server-dev-11 -y
	git clone https://git.taler.net/gnurl.git
	cd gnurl
	sudo ./buildconf
  	sudo ./configure --disable-ftp --disable-file --disable-ldap --disable-rtsp --disable-dict \
	--disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smb --disable-smtp \
	--disable-gopher --without-ssl --without-libpsl --without-librtmp --disable-ntlm-wb 
	sudo make install
	cd ..

# microhttpd:	
	git clone git://git.gnunet.org/libmicrohttpd.git
	cd libmicrohttpd
	sudo ./bootstrap
	sudo ./configure
	sudo make install
	cd ..

# GNUnet:
	sudo apt install git libtool autoconf autopoint build-essential libg20crypt-dev libidn11-dev \
 	  zlib1g-dev libunistring-dev libglpk-dev miniupnpc libextractor-dev libjansson-dev \
	  libcurl4-gnutls-dev gnutls-bin libsqlite3-dev openssl libnss3-tools libmicrohttpd-dev \
	  libopus-dev libpulse-dev libogg-dev libsodium-dev
	git clone git://gnunet.org/git/gnunet.git
	cd gnunet

	./bootstrap
 	export GNUNET_PREFIX=/usr/local # for example, other locations possible
 	export CFLAGS="-g -Wall -O0"
 	./configure --prefix=$GNUNET_PREFIX --disable-documentation --enable-logging=verbose
 	sudo addgroup gnunet
 	sudo usermod -aG gnunet $USER
 	make -j$(nproc || echo -n 1)
 	sudo make install 

#	sudo ./bootstrap
#	sudo ./configure
#	sudo addgroup gnunetdns
#	sudo adduser --system --group --disabled-login --home /var/lib/gnunet gnunet
#	sudo make
#	sudo make install
	cd ..

# Exchange:
	git clone git://git.taler.net/exchange	
	cd exchange	
	sudo ./bootstrap
	sudo ./configure
	sudo make install
	cd ..

# Merchant:
	git clone git://taler.net/merchant
	cd merchant
	sudo ./bootstrap
	sudo ./configure
	sudo make install
