#!/bin/bash
#
# This file is part of TALER
# Copyright (C) 2023, 2024 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with TALER; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>
#
# Author: Christian Grothoff
#
set -eu

function cleanup()
{
    killall fbi
    taler-mdb-display -c "$CONF" -i
    exit 0
}

trap cleanup EXIT

CONF="$HOME/.config/taler.conf"

# Parse command-line options
while getopts ':c:h' OPTION; do
    case "$OPTION" in
        c)
            CONF="$OPTARG"
            ;;
        h)
            echo 'Supported options:'
            # shellcheck disable=SC2016
            echo '  -c $CONF     -- set configuration'
            ;;
        ?)
            exit_fail "Unrecognized command line option"
            ;;
    esac
done
shift $((OPTIND - 1))

taler-mdb-display -c "$CONF"

FBDEV=$(taler-config -c "$CONF" -s taler-mdb -o FRAMEBUFFER_DEVICE)

DATA_HOME=$(taler-config -c "$CONF" -s paths -o DATADIR -f)

fbi -d "$FBDEV" -a -m "768x576-75" -vt 2 -nocomments -noverbose "${DATA_HOME}mdb/$1.png"

while true
do
    sleep 10
done
