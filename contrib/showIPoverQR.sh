#!/bin/sh
# Script for systemd service

# Wait for network to be up
while true
do
 ping -c1 backend.chf.taler.net && break
 sleep 1
done

# Get IP address of 'eth0'
IFC=eth0
IP=`ip -4 -f inet a show dev $IFC | grep inet | awk '{print $2}'`

# Show QR code for 45s, then launch taler-mdb
exec taler-mdb-qr-show -d "15 s" "$IP" -c /home/pi/snack.conf -- taler-mdb -c /home/pi/snack.conf -t -L DEBUG 2>/home/pi/taler.log
