#!/bin/bash
# This file is in the public domain.
# Convert local SVGs to the PNGs for display.

# Please make sure you have the relevant fonts installed - in the case of the builtin SVGs, these are the following:
# All:
#   - Roboto          (Arch: `ttf-roboto`)
# ADs:
#   - Inter           (Arch: `inter-font`)
#   - Source Sans Pro (Arch: `adobe-source-sans-fonts`)
# Err:
#   - JetBrains Mono  (Arch: `ttf-jetbrains-mono`)

set -eu

# General Config
echo "${CONVERSION_TOOL:=inkscape}" &> /dev/null; # Which conversion tool to use - "im" (imagemagick), "gm" (graphicsmagick, can produce weird font results) and "inkscape"
echo "${TARGET_RESOLUTION:="768x576"}" &> /dev/null; # The final output resolution
echo "${MULTITHREAD:=0}" &> /dev/null; # Whether to parallelize it or not - can cause issues with inkscape somehow, idk
echo "${MULTITHREAD_COUNT:=0}" &> /dev/null; # How many instances to run before waiting again, if MULTITHREAD is non-falsey

# Imagemagick/Graphicsmagick Options
echo "${IM_SCALE_RESOLUTION:="1536x1152"}" &> /dev/null; # The resolution we set when telling magick to parse the SVG - higher than TARGET_RESOLUTION due to some blurry imagery caused by imagemagick otherwise.

# Convert a specific image
convertSvgToPng() {
  case "$CONVERSION_TOOL" in
    "im")
      magick convert -density "$TARGET_RESOLUTION" "$1" -resize "$TARGET_RESOLUTION" "$2";
      ;;
    "gm")
      gm convert -density "$TARGET_RESOLUTION" "$1" -resize "$TARGET_RESOLUTION" "$2";
      ;;
    "inkscape")
      inkscape -w "$width" -h "$height" "$1" --export-png-color-mode=RGB_8 -o "$2" 2>&1 | grep -v -E 'data:font/|font-(family|style|weight|display)|unicode-range|end_font_face_cb: font face rule limited support' | sed '/^$/d'
      ;;
    *)
      echo "FATAL: Invalid tool \"$CONVERSION_TOOL\"" 1>&2
      ;;
  esac;
}

# Setup Size Variables
if [[ "$CONVERSION_TOOL" == "inkscape" ]]; then
  width=$(echo -n "$TARGET_RESOLUTION" | sed 's/x/ /' | awk '{print $1}')
  height=$(echo -n "$TARGET_RESOLUTION" | sed 's/x/ /' | awk '{print $2}')
fi;

# Loop over every directory with imagesfor dir in err ads;
ctr=0;
for dir in err ads;
do
  mkdir -p "${dir}/png"

  # Convert them
  for svg in "${dir}/svg/"*;
  do
    png=$(sed 's/svg/png/g' <<< "$svg");
    echo "Converting $svg to $png with $CONVERSION_TOOL";
    if [[ "$MULTITHREAD" != "" ]] && [[ "$MULTITHREAD" != "0" ]] && [[ "$MULTITHREAD" != "no" ]] && [[ "$MULTITHREAD" != "false" ]]; then
      ctr=$((ctr + 1))
      convertSvgToPng "$svg" "$png" &
      if [[ $ctr -gt ${MULTITHREAD_COUNT:=4} ]]; then # Inkscape can crash completely if we run too many instances of it at once
        ctr=0;
        wait;
      fi;
    else
      convertSvgToPng "$svg" "$png";
    fi;
    if which pngnq &>/dev/null; then
      echo "Optimizing $png with pngnq"
      pngnq -s1 < "$png" > "${png}".compact
      mv "${png}".compact "$png"
    else
      echo "Skipping pngnq optimization for $png: pngnq not installed."
    fi;
  done;

  wait;
done;
