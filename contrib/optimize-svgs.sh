#!/bin/zsh
# Calls svgo on all svgs

optimize() {
  svgo -i $1 -o $1 --config svgo.config.mjs
}

for svg in ./**/*.svg; do
  optimize $svg &;
done;
wait;
