export default {
  multipass: true,
  plugins: [
    'preset-default',
    'inlineStyles',
    'mergePaths',
    'mergeStyles',
    'minifyStyles',
    'removeStyleElement',
  ],
};