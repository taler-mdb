/*
     This file is part of GNU Taler.
     Copyright (C) 2019 Taler Systems SA

     Sync is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Sync is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Sync; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file os_installation.c
 * @brief initialize libgnunet OS subsystem for taler-mdb.
 * @author Christian Grothoff
 */
#include "config.h"
#include <gnunet/gnunet_util_lib.h>
#include "taler_mdb_util.h"


/**
 * Default project data used for installation path detection
 * for GNU Taler merchant.
 */
static const struct GNUNET_OS_ProjectData mdb_pd = {
  .libname = "libtalermdbutil",
  .project_dirname = "taler", // FIXME: use taler-mdb!
  .binary_name = "taler-mdb",
  .env_varname = "TALER_MDB_PREFIX",
  .base_config_varname = "TALER_BASE_CONFIG",
  .bug_email = "taler@lists.gnu.org",
  .homepage = "http://www.gnu.org/s/taler/",
  .config_file = "taler-mdb.conf",
  .user_config_file = "~/.config/taler-mdb.conf",
  .version = PACKAGE_VERSION,
  .is_gnu = 1,
  .gettext_domain = "taler-mdb",
  .gettext_path = NULL,
};


/**
 * Return default project data used by Taler merchant.
 */
const struct GNUNET_OS_ProjectData *
TALER_MDB_project_data (void)
{
  return &mdb_pd;
}


/* end of os_installation.c */
